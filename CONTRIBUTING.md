# Main developers:

#### Sebastian Schwalbe, PhD
Email: theonov13@gmail.com
- Coding pragmas; if you do not like it; it is my fault
- Core routines, code structure, and working examples
- Developer of the unrestricted routines (prototypes)
- Code layout, python pragmas/ style
- Object-oriented aspects
    + Atoms class etc.
- Guess: PyCOM routine
- pip package
- Sphinx documentation
- GitLab pages

#### Kai Trepte, PhD
Email: kai.trepte1987@gmail.com
- Developer of the restricted routines
- Main testing, bug reporter  
- Wanted to have: rrun/urun 
- Guess: fodMC routine 
- Systems


#### Wanja Schulze, M. Sc.
Email: wangenau@protonmail.com
- speed-up of FOD forces 
- Make code pep8 conform
- Code testing
- linting 

#### Susi Lethola, PhD
Email: susi.lehtola@helsinki.fi
- Project visionary


# Guests:

#### Jens Kortus, Prof. Dr.
- Theoretical concepts

#### Simon Liebing, PhD
- License issues


# Former developers

#### Lenz Fiedler,  M. Sc.
Email: fiedler.lenz@gmail.com
- All original core FLO-SIC routines
- FOD visualization (e.g., cube files and density plots)
- Basic and advanced tests
- Documentation

