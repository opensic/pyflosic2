**Legacy**

ALPHA_v01: @ theo:/home/SHARE/for_sebastian/pyflosic_master/:
-Freiberg, Germany 10.08.2018 
- combine core pyflosic routines from Lenz Fiedler's master's thesis with the FOD optimization by Sebastian Schwalbe 
- added automatic_guessing.py routine 
- added nrl2py and py2nrl routines 

ALPHA_v02: @ taurus:/projects/p_magmof/pyflosic_clean/: 
- Freiberg, Germany 11.02.2019
- changing the code language from python2.7 to python3
- code cleaning and testing  

GitHub - PyFLOSIC v.1.0.0 release
- Freiberg, Germany 01.05.2020 
- version submitted to ArXiv and JCP 

GitHub - PyFLOSIC v.1.0.1 release
- Freiberg, Germany 18.07.2020
- version resubmitted to ArXiv and JCP (accepted)

GitLab - PyFLOSIC_dev 
- Germany/ USA, 22.10.2021
- merged stable_core_v1.2.0 with master 

GitLab - PyFLOSIC2 
- Germany/ USA, 12.01.2022
- Renamed PyFLOSIC_dev to PyFLOSIC2
- Make a separated repository
- version: v.2.0.0  
