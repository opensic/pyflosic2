![GitLab Logo](/doc/images/PyFLOSIC_two.png)

# PyFLOSIC2 
**Python-based Fermi-Löwdin orbital self-interaction correction (FLO-SIC)**

[![license](https://img.shields.io/badge/license-APACHE2-green)](https://www.apache.org/licenses/LICENSE-2.0)
[![language](https://img.shields.io/badge/language-Python3-blue)](https://www.python.org/)
[![version](https://img.shields.io/badge/version-2.0.0-lightgrey)]()
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5910907.svg)](https://doi.org/10.5281/zenodo.5910907)



[![researchgate](https://img.shields.io/static/v1?label=researchgate&message=OpenSIC&style=social&logo=researchgate)](https://www.researchgate.net/project/Fermi-Loewdin-orbital-self-interaction-correction-developed-in-Freiberg-FLO-SICFG)
[![youtube](https://img.shields.io/static/v1?label=YouTube&message=OpenSIC&logo=youtube&style=social)](https://www.youtube.com/watch?v=-1bxmCwn7Sw)
[![twitter](https://img.shields.io/static/v1?label=twitter&message=OpenSIC&style=social&logo=twitter)](https://twitter.com/OpenSIC_project)

# Installation: Basic

```bash 
pip3 install pyflosic2
```

or      

```bash 
git clone https://gitlab.com/opensic/pyflosic2.git
cd pyflosic2
pip3 install -e .
```

Test the installation. 
```python 
from pyflosic2.sic import flo
```

# Installation: Extras 

TBD

# Example 

Build a python file, e.g., run.py, 
with the following content 

```python
from pyflosic2 import Atoms,WORKFLOW,GUI
# Nuclei
sym = ['C']+4*['H']
p0 = [+0.00000000,+0.00000000,+0.00000000]
p1 = [+0.62912000,+0.62912000,+0.62912000]
p2 = [-0.62912000,-0.62912000,+0.62912000]
p3 = [+0.62912000,-0.62912000,-0.62912000]
p4 = [-0.62912000,+0.62912000,-0.62912000]
pos = [p0,p1,p2,p3,p4]
atoms = Atoms(sym,pos,spin=0,charge=0)
# Workflow
wf = WORKFLOW(atoms,mode='unrestricted',log_name='UWF.log')
wf.kernel(flevel=0)
print(wf.p.atoms,wf.etot)
```
and run in your environment 

```bash 
python3 run.py 
```

# Scripts 
```bash 
pyflosic2 --gui [name].xyz
pyflosic2 --wf unrestricted --xyz [nuclei_only].xyz
pyflosic2 --run unrestricted --xyz [nuclei_and_fods].xyz 
```

## Note!
If you discover any problem while working with the code,
please do not hesitate to contact one of the developers.

## Citation

PyFLOSIC2 is the successor of PyFLOSIC.        
Thus if you currently use the PyFLOSIC2 code   
for a scientific article or contribution,   
please cite the following articles.   

* **PyFLOSIC: Python-based Fermi–Löwdin orbital self-interaction correction**     
  S. Sebastian, L. Fiedler, J. Kraus, J. Kortus, K. Trepte, and S. Lehtola     
  arXiv e-prints, Subject: Computational Physics (physics.comp-ph), 2020, [arXiv:1905.02631](https://arxiv.org/abs/1905.02631)     
  [J. Chem. Phys. 153, 084104, 2020](https://doi.org/10.1063/5.0012519)

* **Effect of molecular and electronic geometries on the electronic density in FLO-SIC**   
  S. Liebing, K. Trepte, and S. Schwalbe       
  arXiv e-prints, Subject: Chemical Physics (physics.chem-ph); Computational Physics (physics.comp-ph), 2022, [arXiv:2201.11648](https://arxiv.org/abs/2201.11648)  
  [Optics and Its Applications. Springer Proceedings in Physics, 281, 2022](https://doi.org/10.1007/978-3-031-11287-4_14)    

* **Bond formation insights into the Diels-Alder reaction: A bond perception and self-interaction perspective**    
  W. T. Schulze, S. Schwalbe, K. Trepte, A. Croy, J. Kortus, and S. Gräfe    
  arXiv e-prints, Subject: Chemical Physics (physics.chem-ph), 2023, [arXiv:2302.02770](https://arxiv.org/abs/2302.02770)    
  [J. Chem. Phys. 158, 164102, 2023](https://doi.org/10.1063/5.0145555)    

* **Ensemble Generalization of the Perdew-Zunger Self-Interaction Correction: a Way Out of Multiple Minima and Symmetry Breaking**   
  S. Schwalbe, W. T. Schulze, K. Trepte, and S. Lehtola    
  arXiv e-prints, Subject: Chemical Physics (physics.chem-ph), 2024 [arXiv:2405.18394](https://arxiv.org/abs/2405.18394)   
  [J. Chem. Theory Comput. 20, 7144, 2024](https://doi.org/10.1021/acs.jctc.4c00694)   
   
# Documentation
Please see the PyFLOSIC2 [documentation](https://opensic.gitlab.io/pyflosic2/) for a detailed introduction.

# Functionality (basic)

| Feature  | Status |
| :-----:  | :----: |  
| RFLO    | Production (code, examples) | 
| UFLO    | Production (code, examples) |
| RFLOSIC | Production (code, examples) | 
| UFLOSIC | Production (code, examples) | 
| RWORKFLOW | Production (code, examples) | 
| UWORKFLOW | Production (code, examples) |
| RRUN | Production (code, examples) |
| URUN | Production (code, examples) |
| UENSEMBLE | Production (code, [examples](https://gitlab.com/opensic/ensemble_supplementary)) |
| guess/pycom |  Production (code, examples) |
| ff/uff | Production (code, examples) | 

# Functionality (extra)

| Feature  | Status | URL  | 
| :-----:  | :----: | :--: | 
| guess: pyfodmc | Under development | [PyfodMC](https://gitlab.com/opensic/PyfodMC)  |
| guess: perception | Production (code, examples) | [PyPERCEPTION](https://gitlab.com/opensic/pyperception) |


