#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
import numpy

from pyflosic2.parameters.flosic_parameters import parameters
from pyflosic2.sic.properties import dip_moment, spin_square
from pyflosic2.sic.uflo import UFLO, make_rdm1
from pyflosic2.sic.uflosic import ufodopt

from pyscf import lib
from pyscf.tools.cubegen import density, orbital


def write_cube(mf, p, mo_coeff, label='orb', n=(80, 80, 80), write_orbitals=True):
    """
        Write Cube files
        ----------------
        Write the localized orbitals as cube files.

        Input
        -----
        mf : mf(), PySCF object
             - carries all PySCF natural variables
        p  : Parameters(), Parameters object/instance
             - carries all PyFLOSIC variables

        Output
        ------
        cube : cube files
    """
    nx, ny, nz = n
    l_cube = []  # list of all cube file names
    den = numpy.zeros_like(p.obj_flo.flo[0][:, 0])
    dm = make_rdm1(mo_coeff, mf.mo_occ, p)
    dm = dm[0] + dm[1]
    density(mf.mol, '{}_den.cube'.format(label), dm, nx=nx, ny=ny, nz=nz)
    if write_orbitals:
        for s in range(p.nspin):
            s_cube = []
            occ = len(mf.mo_coeff[s][mf.mo_occ[s] > 0])
            for i in range(occ):
                f_cube = '{}_orb_{}_spin{}.cube'.format(label, i, s)
                # d_cube = '{}_den.cube'.format(label)
                s_cube.append(f_cube)
                orbital(mf.mol, f_cube, mo_coeff[s][:, i], nx=nx, ny=ny, nz=nz)
                den += abs(mo_coeff[s][:, i])**2
            l_cube.append(s_cube)
        p.l_cube = l_cube
    return p


def kernel_cc_sic(mf, p, P):
    """
        CC-FLO-SIC kernel function
        -----------------------

        if p.optimize_FODs == False
            Density matrix (DM) optimization for fixed Fermi-orbital descriptors (FODs)

        if p.optimize_FODs == True
            Density matrix (DM) optimization and Fermi-orbital descriptors (FODs) optimization

        Input
        -----
        mf: PySCF mf object/instance
        P:  Set of p, PyFLOSIC2 parameters object/instance
    """
    # Stuff
    Nconf = len(P)
    esic = 0
    p.esic = 0
    # Inital density matrix
    mol = mf.mol
    dm = mf.get_init_guess(mol, mf.init_guess)
    # verbose
    mf.verbose = 0

    # The inital energy
    h1e = mf.get_hcore(mol)
    s1e = mf.get_ovlp(mol)
    vhf = mf.get_veff(mol, dm)
    e_tot = mf.energy_tot(dm, h1e, vhf)
    scf_conv = False
    # DIIS
    # gives better SCF convergence
    if isinstance(mf.diis, lib.diis.DIIS):
        mf_diis = mf.diis
    elif mf.diis:
        assert issubclass(mf.DIIS, lib.diis.DIIS)
        mf_diis = mf.DIIS(mf, mf.diis_file)
        mf_diis.space = mf.diis_space
        mf_diis.rollback = mf.diis_space_rollback
    else:
        mf_diis = None

    # Start outer loop
    p.log.init_task('Outer loop', 'optimize DM, fixed FODs')
    for cycle in range(p.max_cycle):
        dm_last = dm
        last_hf_e = e_tot
        # last_esic = p.esic
        # Start inner loop
        # in-scf FODs optimization
        for pi in P:
            if pi.optimize_FODs:
                pi.log.init_task('Inner loop', 'fixed DM, optimize FODs')
                fopt = ufodopt(pi.obj_flo, pi)
                fopt.optimize()
                pi.log.end_task('Inner loop', 'fixed DM, optimize FODs')
                pi.obj_flo.fod1 = fopt.p.fod1
                pi.obj_flo.fod2 = fopt.p.fod2
        # End inner loop
        # DFT part
        fock = mf.get_fock(h1e, s1e, vhf, dm, cycle, mf_diis)
        mo_energy, mo_coeff = mf.eig(fock, s1e)
        mo_occ = mf.get_occ(mo_energy, mo_coeff)
        dm = mf.make_rdm1(mo_coeff, mo_occ)
        dm = lib.tag_array(dm, mo_coeff=mo_coeff, mo_occ=mo_occ)
        vhf = mf.get_veff(mol, dm, dm_last, vhf)
        e_dft = mf.energy_tot(dm, h1e, vhf)
        _ = dip_moment(p, dm)
        _, _ = spin_square(p, mf)
        # norm_ddm = numpy.linalg.norm(dm - dm_last)
        # SIC part
        mf.mo_coeff = mo_coeff
        esic_conf = 0
        vhf_conf = numpy.zeros_like(vhf)
        # Update: SIC properties of each configuration pi
        for pi in P:
            if pi.update_esic:
                e_tot = pi.obj_flo.kernel()
                esic_conf += pi.obj_flo.esic
            vhf_conf += pi.obj_flo.hsic
        # Update: ensemble esic and vhf
        vhf_conf /= Nconf
        esic_conf /= Nconf
        esic = esic_conf
        p.esic = esic
        vhf += vhf_conf
        # Get e_PZ = e_dft(DM_SIC) + esic(DM_SIC)
        e_tot = mf.energy_tot(dm, h1e, vhf) - esic
        p.log.write('>>>> CC-FLO-SIC cycle {} EDFT = {:+.15f} [Ha] <<<<'.format(cycle, e_dft))
        p.log.write('>>>> CC-FLO-SIC cycle {} ESIC = {:+.15f} [Ha] <<<<'.format(cycle, esic))
        p.log.write('>>>> CC-FLO-SIC cycle {} EPZ  = {:+.15f} [Ha] <<<<'.format(cycle, e_tot))
        # in future reconsider gradients
        # and delta density matrix ddm here again
        if abs(e_tot - last_hf_e) < p.conv_tol:
            scf_conv = True
        if scf_conv:
            p.log.end_task('Outer loop', 'optimize DM, fixed FODs')
            p.log.write('The calculation is converged!')
            p.log.write('[Final] CC-FLO-SIC energy = {:.15g} [Ha]'.format(e_tot))
            break
        # End outer loop
    return scf_conv, e_tot, mo_energy, mo_coeff, mo_occ


class UCCFLOSIC():
    """
        The UCCFLOSIC scf class
        ---------------------
        The unrestricted E-FLO-SIC class.

        Examples explaining the usage of E-FLO-SIC
        can be found at
            - https://gitlab.com/opensic/ensemble_supplementary

        If you are publishing results
        generated with this routine
        please cite the following article.

        Reference
            - https://arxiv.org/abs/2405.18394
    """

    def __init__(self, mf, p, cc):
        """
            Initialize class
            ----------------

            Input
            -----
            mf : mf(), PySCF object
                 - carries all PySCF natural variables
            p  : Parameters(), Parameters object/instance
                 - carries all PyFLOSIC variables
            cc : list of coupled configurations (cc)
        """
        # mf carries all PySCF natural variables
        self.mf = mf
        # p carries all PyFLOSIC variables
        self.p = p
        self.cc = cc
        self.P = []
        self.p.log.header('TASK: UFLOSIC')
        # update sic
        self.p.update_esic = True
        # convergence parameter for ESIC
        if not hasattr(self.p, 'conv_esic'):
            self.p.conv_esic = 1e-8
        self.p.show()
        self.write_orbitals = False

    def __init__cc(self):
        """
            __init__cc
            Initialize the configurations pi.
        """
        for i, ci in enumerate(self.cc):
            pi = parameters(mode='unrestricted', log_name=f'UCONFIGURATION{i}.log')
            # Update pi's properties from global p
            for attr in vars(self.p):
                value = getattr(self.p, attr)
                setattr(pi, attr, value)
            pi.opt_fod_name = f'fodopt_config{i}'
            pi.update_esic = True
            pi.init_atoms(ci())
            pi.obj_flo = UFLO(mf=self.mf, p=pi)
            self.P.append(pi)

    def _get_HOMO(self, mo_energy):
        """
            _get_HOMO
            ---------
            Get HOMO.
        """
        Na = len(self.p.fod1)
        Nb = len(self.p.fod2)
        HOMO_a = mo_energy[0][Na - 1]
        if Nb >= Na:
            HOMO_b = mo_energy[0][Nb - 1]
            if HOMO_a < HOMO_b:
                HOMO = HOMO_b
            else:
                HOMO = HOMO_a
        else:
            HOMO = HOMO_a
        return HOMO

    def _get_DENSITY(self):
        """
            _get_DENSITY
            Get the ensemble density.
        """
        write_cube(self.mf, self.P[0], self.mo_coeff, label='ensemble', n=(80, 80, 80),
                   write_orbitals=self.write_orbitals)

    def _get_ORBITALS(self):
        """
            _get_ORBITALS
            -------------
            Get ensemble orbitals.
        """
        for i, pi in enumerate(self.P):
            write_cube(self.mf, pi, pi.obj_flo.flo, label=f'conf{i}', n=(80, 80, 80),
                       write_orbitals=self.write_orbitals)

    def kernel(self):
        """
            Kernel function
            ---------------
            Similar as the PySCF kernel functions.
            Get the FLO-SIC energy in self-consistent field (SCF) cycles.

        """
        self.__init__cc()
        self.scf_conv, self.e_tot, self.mo_energy, self.mo_coeff, self.mo_occ = \
            kernel_cc_sic(self.mf, p=self.p, P=self.P)
        self._get_DENSITY()
        self._get_ORBITALS()
        return self.e_tot

    def __repr__(self):
        """
            Representation
            --------------
            Representation printed e.g. using print(UCCFLOSIC()).
        """
        params = [self.p.tier_name]
        return "UCCFLOSIC('{}')".format(*params)
