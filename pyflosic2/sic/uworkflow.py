#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
from copy import copy
import numpy
from pyflosic2.atoms.atoms import Atoms
from pyscf import gto, scf
from pyflosic2.guess.pycom import pycom
from pyflosic2.io.uflosic_io import atoms2pyscf
from pyflosic2.parameters.flosic_parameters import parameters
from pyflosic2.sic.uflo import UFLO
from pyflosic2.sic.uflosic import get_init_guess_dm, UFLOSIC, ufodopt
from pyflosic2.parameters.flosic_parameters import set_grid
from pyflosic2.sic.chkfile import write_chkfile, load_scf
from pyflosic2.sic.properties import dip_moment, spin_square, ip_unrestricted

def dft(p):
    """
        Calculate: DFT (UKS)
        --------------------
    """
    # header
    p.log.header('Task: DFT (UKS)')
    p.mol = gto.M(atom=atoms2pyscf(p.nuclei), basis=p.basis, spin=p.spin, charge=p.charge, symmetry=p.symmetry)
    mf = scf.UKS(p.mol)
    # SS: b/c different logs
    # SS: PySCF has a different p
    mf.verbose = 0
    # restart options
    if p.save_chk:
        # PySCF chkfile
        # mf.chkfile = p.dft_chk
        mf.diis_file = p.diis_chk
    mf.xc = p.xc
    mf.init_guess_breaksym = False
    mf.conv_tol = p.conv_tol
    mf.conv_tol_grad = p.conv_tol_grad
    # SS: Own handling of grid
    mf = set_grid(mf,value=p.grid_level)
    edft = mf.kernel()
    if p.save_chk:
        # PyFLOSIC DFT chkfile
        # Observables DFT
        dm_dft = mf.make_rdm1(mf.mo_coeff, mf.mo_occ)
        mu_dft = dip_moment(p, dm_dft)
        ss_dft, _ = spin_square(p, mf)
        ip_dft = ip_unrestricted(p, mf)
        dft_dict = {'e_tot'    : mf.e_tot,
                'mo_energy': mf.mo_energy,
                'mo_occ'   : mf.mo_occ,
                'mo_coeff' : mf.mo_coeff,
                'mu'       : mu_dft,
                'ss'       : ss_dft,
                'ip'       : ip_dft}
        write_chkfile(mf,dft_dict,p.dft_chk)
    return p, mf, edft

def restart_dft(p):
    """
        restart_dft
        DFT calculation
    """
    # restart: mo_coeffs from SIC
    restart_mol, restart_scf = load_scf(p.sic_chk)

    # mol
    p.mol = restart_mol

    # DFT
    mf = scf.UKS(p.mol)
    mf.verbose = 0
    mf.xc = p.xc
    mf.init_guess_breaksym = False
    mf.conv_tol = p.conv_tol
    mf.conv_tol_grad = p.conv_tol_grad
    # grid
    mf = set_grid(mf,value=p.grid_level)
    # restart mf object
    mf.__dict__.update(restart_scf)
    # SS: we need to build the grid here b/c
    #     we are not running the kernel
    mf.grids.build()

    # restart: dm
    dm = get_init_guess_dm(mf,p)
    mf.init_guess = dm
    edft = mf.e_tot

    # restart: diis
    mf.diis = scf.diis.DIIS().restore(p.diis_chk)
    return p, mf, edft

def guess(mf, p):
    """
        Generate: initial FODs (guess)
        ------------------------------
        Currently using PyCOM procedure.
    """
    # header
    p.log.header('Task: PyCOM')
    p.pycom_loc = ['PM', 'FB'][1]
    p.write_cubes = [True, False][1]
    pc = pycom(mf=mf, p=p)
    pc.get_guess()
    pos_fod1 = p.l_com[0]
    pos_fod2 = p.l_com[1]
    p.fod1 = Atoms(len(pos_fod1) * [p.sym_fod1], pos_fod1)
    p.fod2 = Atoms(len(pos_fod2) * [p.sym_fod2], pos_fod2)
    p.atoms = p.nuclei + p.fod1 + p.fod2

def restart_guess(mf, p):
    """
        Restart FODs
        ------------------------------
    """
    # header
    p.log.header('Task: Restart FODs')
    p.fod1 = Atoms(len(mf.pos_fod1) * [p.sym_fod1], mf.pos_fod1)
    p.fod2 = Atoms(len(mf.pos_fod2) * [p.sym_fod2], mf.pos_fod2)
    p.atoms = p.nuclei + p.fod1 + p.fod2

def flosic_level0(mf, p):
    """
        FLO-SIC(level=0)
        ----------------
        Density matrix (DM) for inital fixed Fermi orbital descriptors (FODs)
        DM : not optimized, inital DFT
        FODs: not optimized, inital FODs
    """
    # header
    p.log.header('Task: FLO-SIC(level=0)')
    p.show()
    flo = UFLO(mf=mf, p=p)
    flo.kernel()
    etot = flo.e_tot
    # SS: to be sure we do not influence other calcs
    del flo
    return etot


def flosic_level1(mf, p):
    """
        FLO-SIC(level=1)
        ----------------
        Density matrix (DM) for inital fixed Fermi orbital descriptors (FODs)
        DM : optimized
        FODs: not optimized
    """
    # header
    p.log.header('Task: FLO-SIC(level=1)')
    p.optimize_FODs = False
    mflo = UFLOSIC(mf=mf, p=p)
    etot = mflo.kernel()
    # SS: to be sure we do not influence other calcs
    del mflo
    return etot


def flosic_level2(mf, p):
    """
        FLO-SIC(level=2)
        ----------------
        Density matrix (DM) for Fermi orbital descriptors (FODs, optimized for DM_init)
        step1: DM_init: see opt_dm_fixed_fods
        step2: optimized FODs for fixed DM_init -> FODs(DM_init)
        step3: optimized DM for FODs(DM_init)
    """
    # header
    p.log.header('Task: FLO-SIC(level=2)')
    # Step1: DM for init FODs
    # Note: Step1 is may equal to flosic_level1
    p.optimize_FODs = False
    mflo = UFLOSIC(mf=mf, p=p)
    etot = mflo.kernel()
    # Step2: opt FODs for DM
    fopt = ufodopt(mflo, p)
    fopt.optimize()
    # Step3: opt DM for opt FODs
    # FLO-SIC fixed pre-optimized FODs and optimize DM
    p.optimize_FODs = False
    mflo = UFLOSIC(mf=mf, p=p)
    etot = mflo.kernel()
    # SS: to be sure we do not influence other calcs
    del mflo
    return etot


def flosic_level3(mf, p):
    """
        FLO-SIC(level=3)
        ----------------
        Density matrix (DM) for Fermi orbital descriptors (FODs)
        Repeat outer and inner loop until DM is converged (SCF thresholds)
        and FODs are not changing (fmax).
        Tags: full-self-consistent (SCF) FLO-SIC, in-scf FLO-SIC

        outer loop: DM optimized for current FODs
        inner loop: FODs optimized for current DM

    """
    # header
    p.log.header('Task: FLO-SIC(level=3)')
    p.optimize_FODs = True
    mflo = UFLOSIC(mf=mf, p=p)
    etot = mflo.kernel()
    etot_init = mflo.e_tot_init
    if p.save_chk:
        dm_sic = mf.make_rdm1(mflo.mo_coeff, mflo.mo_occ)
        mu_sic = dip_moment(p, dm_sic)
        ss_sic, _ = spin_square(p, mflo.mf)
        ip_sic = ip_unrestricted(p, mflo.mf)
        pos_fod1 = numpy.array(mflo.p.obj_flo.p.fod1.get_positions()) #.flatten()
        pos_fod2 = numpy.array(mflo.p.obj_flo.p.fod2.get_positions()) #.flatten()
        sic_dict = {'e_tot'    : mflo.e_tot,
                    'mo_energy': mflo.mo_energy,
                    'mo_occ'   : mflo.mo_occ,
                    'mo_coeff' : mflo.mo_coeff,
                    'mu'       : mu_sic,
                    'ss'       : ss_sic,
                    'ip'       : ip_sic,
                    'pos_fod1' : pos_fod1,
                    'pos_fod2' : pos_fod2}

        write_chkfile(mflo.mf,sic_dict,p.sic_chk)

    # SS: to be sure we do not influence other calcs
    del mflo
    return etot, etot_init


class UWORKFLOW():
    """
        UWORKFLOW class
        ---------------
        Performs an automatic unrestricted FLO-SIC calculation
        from scratch, i.e.,
        only providing chemical symbols and positions
        of the nuclei.
        Main function to be used is the kernel function.
    """
    def __init__(self, atoms, **kwargs):
        """
            Initialize class
            ----------------

            Input
            -----
            - system information
                atoms: atoms, nuclei only
                spin: int(), 2S = Nalpha - Nbeta, spin
                charge: int(), charge of the system
            - calculation information
                sym_fod1: str(), symbol alpha FODs
                sym_fod2: str(), symbol beta FODs
                tier_name: str(), default numerical parameter levels, e.g., 'tier1'
                flevel: int(), FLO-SIC approximation (0-3), (low - high accuracy)
                log_name: str(), output log file name
        """
        # Get secondary input parameters
        self._set_kwargs(kwargs)
        # Parameters instance (p)
        self.p = parameters(mode='unrestricted',
                            tier_name=self.tier_name,
                            log_name=self.log_name)
        self.p.xc = self.xc
        self.p.restart = self.restart
        if self.restart:
            self.p.load_chk = True
        self.p.dft_chk = self.dft_chk
        self.p.sic_chk = self.sic_chk
        self.p.diis_chk = self.diis_chk
        self.p.max_cycle = self.max_cycle
        self.p.nuclei = atoms
        self.p.spin = atoms._spin
        self.p.charge = atoms._charge
        self.p.sym_fod1 = atoms._elec_symbols[0]
        self.p.sym_fod2 = atoms._elec_symbols[1]
        self.p.symmetry = False
        self.p.flevel = self.flevel
        # Setup
        self.setup()

    def _set_kwargs(self,kwargs):
        """
            _set_kwargs
            Set secondary input parameters.
            If not set from the user,
            default values will be used.
        """
        self.tier_name = kwargs.get("tier_name","tier1")
        self.flevel = kwargs.get("flevel",0)
        self.log_name = kwargs.get("log_name","pyflosic2.log")
        self.xc = kwargs.get("xc","LDA,PW")
        self.restart = kwargs.get("restart",False)
        self.max_cycle = kwargs.get("max_cycle",300)
        self.dft_chk = kwargs.get("dft_chk","dft.chk")
        self.sic_chk = kwargs.get("sic_chk","sic.chk")
        self.diis_chk = kwargs.get("diis_chk","diis.chk")

    def dft(self):
        """
            Calculatue: DFT
            ---------------
        """
        if self.p.restart:
            self.p, self.mf, self.edft = restart_dft(self.p)
        else:
            self.p, self.mf, self.edft = dft(self.p)
        # Reference for mf: mf0
        self.mf0 = copy(self.mf)
        self.p.log.write('EDFT = {} [Ha]'.format(self.edft))

    def guess(self):
        """
            Get: FOD guess
            --------------
        """
        if self.p.restart:
            restart_guess(self.mf,self.p)
        else:
            guess(self.mf, self.p)

    def atoms(self):
        """
            Get: atoms
            -----------
        """
        self.p.init_atoms(self.p.atoms)
        # Reference for p: p0
        self.p0 = copy(self.p)

    def setup(self):
        """
            Setup: Nuclei and inital FODs
            -----------------------------
        """
        self.dft()
        self.guess()
        self.atoms()

    def flosic(self, update, flevel=None):
        """
            Calculate: FLO-SIC
            ------------------
        """
        if flevel is not None:
            self.p.flevel = flevel
        if self.p.flevel == 0:
            self.etot = flosic_level0(self.mf, self.p)
        if self.p.flevel == 1:
            self.etot = flosic_level1(self.mf, self.p)
        if self.p.flevel == 2:
            self.etot = flosic_level2(self.mf, self.p)
        if self.p.flevel == 3:
            self.etot, self.etot_init = flosic_level3(self.mf, self.p)
        self.p.log.write('EFLOSIC(level={}) = {} [Ha]'.format(self.p.flevel, self.etot))
        if not update:
            self.reset()

    def reset(self):
        """
            Restore starting point
            ----------------------
            This can be useful to produce correct timings.
            If one aims to compare different flevel.
        """
        self.mf = copy(self.mf0)
        self.p = copy(self.p0)

    def kernel(self, update=True, flevel=None):
        """
            Kernel function
            ---------------
        """
        self.flosic(update=update, flevel=flevel)
        return self.etot


if __name__ == '__main__':
    # Nuclei
    sym = 'C' + 4 * 'H'
    p0 = [+0.00000000, +0.00000000, +0.00000000]
    p1 = [+0.62912000, +0.62912000, +0.62912000]
    p2 = [-0.62912000, -0.62912000, +0.62912000]
    p3 = [+0.62912000, -0.62912000, -0.62912000]
    p4 = [-0.62912000, +0.62912000, -0.62912000]
    pos = [p0, p1, p2, p3, p4]
    # System: information
    charge = 0
    spin = 0
    atoms = Atoms(sym, pos, spin=spin, charge=charge)
    # Workflow
    uwf = UWORKFLOW(atoms)
    # FLO-SIC approximations
    uwf.kernel(update=False, flevel=0)
    uwf.kernel(update=False, flevel=1)
    uwf.kernel(update=False, flevel=2)
    uwf.kernel(update=False, flevel=3)
