#!/usr/bin/env python
# Copyright 2020-2024 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Wanja Timm Schulze <wangenau@protonmail.com>
#
from pyflosic2.sic.uensemble import UCCFLOSIC
from pyflosic2.sic.rensemble import RCCFLOSIC


def CCFLOSIC(mf, p, cc):
    """
        PyFLOSIC2: CCFLOSIC
        --------------
        Use conditional inheritance to handle
        different calculation modi.
    """
    if p.mode == 'unrestricted':
        return UCCFLOSIC(mf, p, cc)
    if p.mode == 'restricted':
        return RCCFLOSIC(mf, p, cc)
