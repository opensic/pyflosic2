import h5py
from pyscf.lib.chkfile import load
from pyscf.lib.chkfile import save
from pyscf.lib.chkfile import load_mol, save_mol

def load_scf(chkfile):
    return load_mol(chkfile), load(chkfile, 'scf')

def dump_scf(mol, chkfile, results_dict, overwrite_mol=True):
    """
        dump_scf 
        save temporary results
    """
    if h5py.is_hdf5(chkfile) and not overwrite_mol:
        with h5py.File(chkfile, 'a') as fh5:
            if 'mol' not in fh5:
                fh5['mol'] = mol.dumps()
    else:
        save_mol(mol, chkfile)

    save(chkfile, 'scf', results_dict)

def write_chkfile(mf,results_dict,name):
    """
        write_chkfile
        Write chkfile for DFT or E-FLO-SIC.
    """
    dump_scf(mf.mol,
             name,
             results_dict)

