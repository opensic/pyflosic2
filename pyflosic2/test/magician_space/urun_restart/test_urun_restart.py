#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
import numpy
from pyflosic2.sic.urun import URUN
from pyflosic2.parameters.flosic_parameters import parameters
from pyflosic2.systems.uflosic_systems import CH4
from pyflosic2.io.uflosic_io import read_flosic_xyz

def test_urun_restart():
    """
        test_urun_restart
        
        This examples tests the restart option of PyFLOSIC2 
        URUN. 
        
        We run a full URUN calculation and use it 
        as reference. This calculation is refered 
        to as the "full" pyflosic2 calculation. 
        
        Afterwards, we run the same calculation, but 
        end it after a fixed number of max_cycle. 
        This calculation is refered to as the 
        "broken" calculation. 
        
        For the third calculation we reload 
        the information of the "broken" calculation 
        and restart URUN from the latest 
        values of the "broken" calculation. 
        The third calculation is refered to 
        as "restart" calculation. 
        
        The test checks if the initial energy 
        of the restart calculation 
        agrees with the final energy of 
        the broken calculation.   
        
        Note
            - atoms are updated by atoms object       
            - typically the full calculation needs under the current settings (tier2) 6 iterations 
            - however, sometimes it takes more 
            - thus the final values for restart and full are may different in same cases 
    """
    prefix = "urun"
    # System: information
    atoms = CH4()
    # break the cycle 
    max_cycle = 3 
    # Workflow
    uwf = URUN(atoms,
               tier_name="tier2",
               log_name=f'{prefix}_full.log',
               dft_chk=f'{prefix}_dft.chk',
               sic_chk=f'{prefix}_sic.chk',
               diis_chk=f'{prefix}_diis.chk')               

    # full calculation 
    etot_full = uwf.kernel(update=False, flevel=3)
    # broken calculation 
    uwf_break = URUN(atoms,
                     tier_name="tier2",
                     log_name=f'{prefix}_break.log',
                     dft_chk=f'{prefix}_dft.chk',
                     sic_chk=f'{prefix}_sic.chk',
                     diis_chk=f'{prefix}_diis.chk',
                     max_cycle=max_cycle) 
    etot_break = uwf_break.kernel(update=False, flevel=3)
    # restarted calculation
    atoms_restart, _, _ = read_flosic_xyz("fodopt.xyz") 
    uwf_restart = URUN(atoms_restart,
                       tier_name="tier2",
                       log_name=f'{prefix}_restart.log',
                       dft_chk=f'{prefix}_dft.chk',
                       sic_chk=f'{prefix}_sic.chk',
                       diis_chk=f'{prefix}_diis.chk',
                       restart=True) 
    etot_restart = uwf_restart.kernel(update=False, flevel=3)
    etot_init_restart = uwf_restart.etot_init 
    
    # Summarize the results  
    p = parameters(mode='unrestricted',  
                   log_name=f'{prefix}.log')
    p.log.write(f"{etot_full=}")
    p.log.write(f"{etot_break=}")
    p.log.write(f"{etot_init_restart=}")
    p.log.write(f"{etot_restart=}")
    assert numpy.isclose(etot_init_restart, etot_break)
    p.log.write('Tests: passed [okay]')
    
if __name__ == '__main__':
    test_urun_restart()
