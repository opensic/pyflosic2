#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
import numpy
from pyflosic2.sic.uworkflow import Atoms, UWORKFLOW
from pyflosic2.parameters.flosic_parameters import parameters

def test_uworkflow_restart():
    """
        test_uworkflow_restart
        This examples tests the restart option of PyFLOSIC2 
        uworkflow. 
        
        We run a full UWORKFLOW calculation and use it 
        as reference. This calculation is refered 
        to as the "full" pyflosic2 calculation. 
        
        Afterwards, we run the same calculation, but 
        end it after a fixed number of max_cycle. 
        This calculation is refered to as the 
        "broken" calculation. 
        
        For the third calculation we reload 
        the information of the "broken" calculation 
        and restart UWORKFLOW from the latest 
        values of the "broken" calculation. 
        The third calculation is refered to 
        as "restart" calculation. 
        
        The test checks if the initial energy 
        of the restart calculation 
        agrees with the final energy of 
        the broken calculation.     
        
        Note 
            - atoms (nuclei + fods) are updated from chkfile 
            - typically the full calculation needs under the current settings (tier2) 6 iterations 
            - however, sometimes it takes more 
            - thus the final values for restart and full are may different in same cases 
        
    """
    prefix = "uworkflow"
    # Nuclei
    sym = ['C'] + 4 * ['H']
    p0 = [+0.00000000, +0.00000000, +0.00000000]
    p1 = [+0.62912000, +0.62912000, +0.62912000]
    p2 = [-0.62912000, -0.62912000, +0.62912000]
    p3 = [+0.62912000, -0.62912000, -0.62912000]
    p4 = [-0.62912000, +0.62912000, -0.62912000]
    pos = [p0, p1, p2, p3, p4]
    # System: information
    charge = 0
    spin = 0
    atoms = Atoms(sym, pos, spin=spin, charge=charge)
    # break the cycle 
    max_cycle = 3 
    # Workflow
    # full calculation 
    uwf = UWORKFLOW(atoms,
                    tier_name="tier2",
                    log_name=f'{prefix}_full.log',
                    dft_chk=f'{prefix}_dft.chk',
                    sic_chk=f'{prefix}_sic.chk',
                    diis_chk=f'{prefix}_diis.chk') 
    etot_full = uwf.kernel(update=False, flevel=3)
    # broken calculation 
    uwf_break = UWORKFLOW(atoms,
                          tier_name="tier2",
                          log_name=f'{prefix}_break.log',
                          dft_chk=f'{prefix}_dft.chk',
                          sic_chk=f'{prefix}_sic.chk',
                          diis_chk=f'{prefix}_diis.chk',
                          max_cycle=max_cycle) 
    etot_break = uwf_break.kernel(update=False, flevel=3)
    # restarted calculation 
    uwf_restart = UWORKFLOW(atoms,
                            tier_name="tier2",
                            log_name=f'{prefix}_restart.log',
                            dft_chk=f'{prefix}_dft.chk',
                            sic_chk=f'{prefix}_sic.chk',
                            diis_chk=f'{prefix}_diis.chk',
                            restart=True) 
    etot_restart = uwf_restart.kernel(update=False, flevel=3)
    etot_init_restart = uwf_restart.etot_init 
    
    # Summarize the results  
    p = parameters(mode='unrestricted',  
                   log_name=f'{prefix}.log')
    p.log.write(f"{etot_full=}")
    p.log.write(f"{etot_break=}")
    p.log.write(f"{etot_init_restart=}")
    p.log.write(f"{etot_restart=}")
    assert numpy.isclose(etot_init_restart, etot_break)
    p.log.write('Tests: passed [okay]')

if __name__ == '__main__':
    test_uworkflow_restart()
