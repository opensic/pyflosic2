#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
from pyflosic2.test.magician_space.uworkflow_restart.test_uworkflow_restart import test_uworkflow_restart
# from pyflosic2.test.magician_space.urun_restart.test_urun_restart import test_urun_restart

def main():
    """
        main
        ----
        Main function to test PyFLOSIC2 functionalities

        Note
        ----
            - does not include workflow as there is not a unique FOD starting point
            - testing calculations are performed with low numerics (low tier level and low flevel)
            - thus results for full optimization may differ
    """
    test_uworkflow_restart()
    # test_urun_restart()

if __name__ == '__main__':
    main()
