#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
import os

for prefix in  ["urun","uworkflow"]: 
    delete = ['fodopt.xyz', 
              f'{prefix}.log',
              f'{prefix}_full.log',
              f'{prefix}_break.log',
              f'{prefix}_restart.log',
              'UFLO.log', 
              'RFLO.log', 
              'FB_GUESS_COM.xyz',
              f'{prefix}_diis.chk',
              f'{prefix}_dft.chk',
              f'{prefix}_sic.chk']
    
    for d in delete:
        try:
            os.remove(d)
            print(f"File {d} Removed!")
        except BaseException:
            'Nothing'
