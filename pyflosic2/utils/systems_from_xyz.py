#!/usr/bin/env python
# Copyright 2020-2022 The PyFLOSIC Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Author: Sebastian Schwalbe <theonov13@gmail.com>
#
import sys
import numpy
from pyflosic2 import Atoms
from pyflosic2.io.flosic_io import read_xyz, atoms2flosic

def sort_sym_pos(sym,pos,spin,charge,sym_fod1,sym_fod2):
    """
        sort_sym_pos
        Sort nuclei symbols and positions
        to enable a nicer looking symbols list for
        gen_systems_from_xyz.
    """
    atoms = Atoms(sym, pos, spin=spin, charge=charge, elec_symbols=[sym_fod1,sym_fod2])
    [nuclei, fod1, fod2] = atoms2flosic(atoms, sym_fod1=sym_fod1, sym_fod2=sym_fod2)
    sym = numpy.array(nuclei._symbols)
    pos = numpy.array(nuclei._positions)
    idx = sym.argsort()
    sym = sym[idx]
    pos = pos[idx]
    sym = sym.tolist()
    pos = pos.tolist()
    sym.extend(fod1._symbols)
    sym.extend(fod2._symbols)
    pos.extend(fod1._positions)
    pos.extend(fod2._positions)
    return sym, pos

def gen_systems_from_xyz(f_name, charge=0, spin=0,name=None,stream=None):
    """
        Generate: System information from xyz files
        -------------------------------------------
        Powered by lazyness.
        Please use with great care!
    """
    if stream is None:
        stream = sys.stdout
    if name is None:
        name = f_name.split('.')[0]
    print(f'def {name}():',file=stream)
    print('    """',file=stream)
    print(f'        {name} example',file=stream)
    print('    """',file=stream)
    sym, pos, sym_fod1, sym_fod2 = read_xyz(f_name)
    # Sort nuclei symbols and positions
    sym, pos = sort_sym_pos(sym,pos,spin,charge,sym_fod1,sym_fod2)
    values, index, counts = numpy.unique(sym, return_counts=True, return_index=True)
    # Note: numpy.unique sorts everything internally
    # We need to reconstruct the correct order.
    idx = index.argsort()
    values = values[idx]
    counts = counts[idx]
    s = ''
    for v, w in zip(values, counts):
        s += f"['{v}']*{w}+"
    s = s[:-1]
    print(f"    sym = {s}",file=stream)
    ptot = '['
    for i, p in enumerate(pos):
        print(f'    p{i} = [{p[0]},{p[1]},{p[2]}]',file=stream)
        ptot += f"p{i},"
    ptot = ptot[:-1] + ']'
    print(f'    pos = {ptot}',file=stream)
    print(f'    charge = {charge}',file=stream)
    print(f'    spin = {spin}',file=stream)
    print(f"    atoms = Atoms(sym,pos,charge=charge,spin=spin,elec_symbols=['{sym_fod1}','{sym_fod2}'])",file=stream)
    print("    return atoms",file=stream)


if __name__ == '__main__':
    f_name = "C6H6_LDQ1.xyz"
    with open("systems.py","w") as stream:
        gen_systems_from_xyz(f_name=f_name,
                             charge=0,
                             spin=1,
                             name="C6H6",
                             stream=stream)
