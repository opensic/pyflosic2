.. table:: Tier levels (tier). All tier levels use unpruned
grids.[tab:tiers]

   +------+---------+--------------+-------------------------+
   | Tier | Basis   | Grid         | Meaning                 |
   +======+=========+==============+=========================+
   | 1    | STO-3G  | grid.level=3 | code debugging          |
   +------+---------+--------------+-------------------------+
   | 2    | pc-0    | grid.level=3 | examplary calculation   |
   +------+---------+--------------+-------------------------+
   | 3    | pc-1    | grid.level=7 | useful calculation      |
   +------+---------+--------------+-------------------------+
   | 4    | pc-2    | (150,1202)   | production quality      |
   +------+---------+--------------+-------------------------+
   | 5    | aug-pc2 | (200,1454)   | high quality            |
   +------+---------+--------------+-------------------------+

.. raw:: latex

   \small 

.. table:: FLO-SIC levels (Flevel). Here init stands for the values the
calculation is initialized with, whereas opt refers to the fact that
these properties are fully optimized. Update refers to one property
being adjusted with respect to the other property. The meaning is
expressed as functional dependencies.[tab:flevels]

   +--------+--------+--------+--------------------------------------------+
   | Flevel | FODs   | DM     | Meaning                                    |
   +========+========+========+============================================+
   | 0      | init   | init   | FLO(DM\ :math:`_{\text{init}}`)            |
   +--------+--------+--------+--------------------------------------------+
   | 1      | init   | update | DM(FOD\ :math:`_{\text{init}}`)            |
   +--------+--------+--------+--------------------------------------------+
   | 2      | update | update | DM(FOD(DM\ :math:`_{\text{init}}`))        |
   +--------+--------+--------+--------------------------------------------+
   | 3      | opt    | opt    | SCF(DM\ :math:`_{i}`\ (FOD:math:`_{j}`))   |
   +--------+--------+--------+--------------------------------------------+
