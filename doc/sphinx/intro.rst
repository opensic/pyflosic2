.. _intro:


***************
Background
***************

.. _installing-docdir:
Density functional theory (DFT) has become one of 
the standard methods in computational
materials science, condensed matter physics
as well as chemistry thanks to its combination of reasonable
accuracy and computational efficiency. However,
currently available density functional approximations
(DFAs) are well-known to fail in a number of rather simple
situations. Many of these shortcomings come from the fact that DFAs describe
spurious interactions of electrons with themselves,
known as the self-interaction error (SIE). This SIE causes the electron
density to delocalize. To combat this error, various self-interaction
corrections (SICs) were developed, and their usefulness has been demonstrated.
Here, we want to focus on the SIC approach by Perdew and Zunger :cite:`Perdew1981_5048`
and the Fermi-Löwdin orbital-based SIC by Pederson and coworkers :cite:`Pederson2014_121103`.

Perdew-Zunger self-interaction correction (PZ-SIC)
===================================================

The Kohn-Sham (KS) DFT functional reads

:math:`E[n^{\alpha},n^{\beta}] = T_{\text{s}}[n^{\alpha},n^{\beta}]+ V[n] + J[n] + K[n^{\alpha},n^{\beta}]`

The Self-interaction energy for one-electron densities is defined as

:math:`E_{\text{SI}}[n_{1}^{\sigma}] = K[n_{1}^{\sigma},0] + J[n_{1}^{\sigma}]`

The PZ-SIC functional, correcting the one-electron SIE, reads

:math:`E_{\text{PZ}} = E_{\text{KS}}[n^{\alpha},n^{\beta}] - \sum_{\sigma} \sum_{i}^{N^{\sigma}} E_{\text{SI}}[n_{i}^{\sigma}]`

Fermi-Löwdin orbital self-interaction correction
================================================

In the Fermi-Löwdin orbital SIC (FLO-SIC), localized orbitals 
are generated from Fermi orbitals, which are orthogonalized
using Löwdin's method. The Fermi orbitals are based on points
in space called Fermi-orbital descriptors (FODs). Every electron
is associated with one FOD, leading to one localized orbital.

For further aspects of the FLO-SIC theory itself 
consider :cite:`Schwalbe2020_084104`.
In the article "Interpretation and automatic generation 
of Fermi-orbital descriptors" :cite:`Schwalbe2019_2843`, 
various aspects of Fermi-orbital descriptors (FODs) as 
essential building blocks within FLO-SIC are discussed.
Further aspects regarding how FODs can be used to guide 
SIC solutions are discussed in :cite:`Trepte2021_224109`.


