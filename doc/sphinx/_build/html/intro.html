<!DOCTYPE html>
<html class="writer-html5" lang="en" >
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>3. Background &mdash; PyFLOSIC2  documentation</title>
      <link rel="stylesheet" href="_static/pygments.css" type="text/css" />
      <link rel="stylesheet" href="_static/css/theme.css" type="text/css" />
      <link rel="stylesheet" href="_static/graphviz.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="_static/js/html5shiv.min.js"></script>
  <![endif]-->
  
        <script data-url_root="./" id="documentation_options" src="_static/documentation_options.js"></script>
        <script src="_static/jquery.js"></script>
        <script src="_static/underscore.js"></script>
        <script src="_static/doctools.js"></script>
        <script async="async" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script src="_static/js/theme.js"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="4. PyFLOSIC2" href="pyflosic2.html" />
    <link rel="prev" title="2. Examples" href="examples.html" /> 
</head>

<body class="wy-body-for-nav"> 
  <div class="wy-grid-for-nav">
    <nav data-toggle="wy-nav-shift" class="wy-nav-side">
      <div class="wy-side-scroll">
        <div class="wy-side-nav-search" >
            <a href="index.html" class="icon icon-home"> PyFLOSIC2
          </a>
<div role="search">
  <form id="rtd-search-form" class="wy-form" action="search.html" method="get">
    <input type="text" name="q" placeholder="Search docs" />
    <input type="hidden" name="check_keywords" value="yes" />
    <input type="hidden" name="area" value="default" />
  </form>
</div>
        </div><div class="wy-menu wy-menu-vertical" data-spy="affix" role="navigation" aria-label="Navigation menu">
              <ul class="current">
<li class="toctree-l1"><a class="reference internal" href="install.html">1. Installation</a></li>
<li class="toctree-l1"><a class="reference internal" href="examples.html">2. Examples</a></li>
<li class="toctree-l1 current"><a class="current reference internal" href="#">3. Background</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#perdew-zunger-self-interaction-correction-pz-sic">3.1. Perdew-Zunger self-interaction correction (PZ-SIC)</a></li>
<li class="toctree-l2"><a class="reference internal" href="#fermi-lowdin-orbital-self-interaction-correction">3.2. Fermi-Löwdin orbital self-interaction correction</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="pyflosic2.html">4. PyFLOSIC2</a></li>
<li class="toctree-l1"><a class="reference internal" href="literature.html">5. Literature</a></li>
<li class="toctree-l1"><a class="reference internal" href="api.html">6. API</a></li>
</ul>

        </div>
      </div>
    </nav>

    <section data-toggle="wy-nav-shift" class="wy-nav-content-wrap"><nav class="wy-nav-top" aria-label="Mobile navigation menu" >
          <i data-toggle="wy-nav-top" class="fa fa-bars"></i>
          <a href="index.html">PyFLOSIC2</a>
      </nav>

      <div class="wy-nav-content">
        <div class="rst-content">
          <div role="navigation" aria-label="Page navigation">
  <ul class="wy-breadcrumbs">
      <li><a href="index.html" class="icon icon-home"></a> &raquo;</li>
      <li><span class="section-number">3. </span>Background</li>
      <li class="wy-breadcrumbs-aside">
            <a href="_sources/intro.rst.txt" rel="nofollow"> View page source</a>
      </li>
  </ul>
  <hr/>
</div>
          <div role="main" class="document" itemscope="itemscope" itemtype="http://schema.org/Article">
           <div itemprop="articleBody">
             
  <div class="section" id="background">
<span id="intro"></span><h1><span class="section-number">3. </span>Background<a class="headerlink" href="#background" title="Permalink to this headline">¶</a></h1>
<p>Density functional theory (DFT) has become one of
the standard methods in computational
materials science, condensed matter physics
as well as chemistry thanks to its combination of reasonable
accuracy and computational efficiency. However,
currently available density functional approximations
(DFAs) are well-known to fail in a number of rather simple
situations. Many of these shortcomings come from the fact that DFAs describe
spurious interactions of electrons with themselves,
known as the self-interaction error (SIE). This SIE causes the electron
density to delocalize. To combat this error, various self-interaction
corrections (SICs) were developed, and their usefulness has been demonstrated.
Here, we want to focus on the SIC approach by Perdew and Zunger <a class="bibtex reference internal" href="literature.html#perdew1981-5048" id="id1">[1]</a>
and the Fermi-Löwdin orbital-based SIC by Pederson and coworkers <a class="bibtex reference internal" href="literature.html#pederson2014-121103" id="id2">[2]</a>.</p>
<div class="section" id="perdew-zunger-self-interaction-correction-pz-sic">
<h2><span class="section-number">3.1. </span>Perdew-Zunger self-interaction correction (PZ-SIC)<a class="headerlink" href="#perdew-zunger-self-interaction-correction-pz-sic" title="Permalink to this headline">¶</a></h2>
<p>The Kohn-Sham (KS) DFT functional reads</p>
<p><span class="math notranslate nohighlight">\(E[n^{\alpha},n^{\beta}] = T_{\text{s}}[n^{\alpha},n^{\beta}]+ V[n] + J[n] + K[n^{\alpha},n^{\beta}]\)</span></p>
<p>The Self-interaction energy for one-electron densities is defined as</p>
<p><span class="math notranslate nohighlight">\(E_{\text{SI}}[n_{1}^{\sigma}] = K[n_{1}^{\sigma},0] + J[n_{1}^{\sigma}]\)</span></p>
<p>The PZ-SIC functional, correcting the one-electron SIE, reads</p>
<p><span class="math notranslate nohighlight">\(E_{\text{PZ}} = E_{\text{KS}}[n^{\alpha},n^{\beta}] - \sum_{\sigma} \sum_{i}^{N^{\sigma}} E_{\text{SI}}[n_{i}^{\sigma}]\)</span></p>
</div>
<div class="section" id="fermi-lowdin-orbital-self-interaction-correction">
<h2><span class="section-number">3.2. </span>Fermi-Löwdin orbital self-interaction correction<a class="headerlink" href="#fermi-lowdin-orbital-self-interaction-correction" title="Permalink to this headline">¶</a></h2>
<p>In the Fermi-Löwdin orbital SIC (FLO-SIC), localized orbitals
are generated from Fermi orbitals, which are orthogonalized
using Löwdin’s method. The Fermi orbitals are based on points
in space called Fermi-orbital descriptors (FODs). Every electron
is associated with one FOD, leading to one localized orbital.</p>
<p>For further aspects of the FLO-SIC theory itself
consider <a class="bibtex reference internal" href="literature.html#schwalbe2020-084104" id="id3">[3]</a>.
In the article “Interpretation and automatic generation
of Fermi-orbital descriptors” <a class="bibtex reference internal" href="literature.html#schwalbe2019-2843" id="id4">[4]</a>,
various aspects of Fermi-orbital descriptors (FODs) as
essential building blocks within FLO-SIC are discussed.
Further aspects regarding how FODs can be used to guide
SIC solutions are discussed in <a class="bibtex reference internal" href="literature.html#trepte2021-224109" id="id5">[5]</a>.</p>
</div>
</div>


           </div>
          </div>
          <footer><div class="rst-footer-buttons" role="navigation" aria-label="Footer">
        <a href="examples.html" class="btn btn-neutral float-left" title="2. Examples" accesskey="p" rel="prev"><span class="fa fa-arrow-circle-left" aria-hidden="true"></span> Previous</a>
        <a href="pyflosic2.html" class="btn btn-neutral float-right" title="4. PyFLOSIC2" accesskey="n" rel="next">Next <span class="fa fa-arrow-circle-right" aria-hidden="true"></span></a>
    </div>

  <hr/>

  <div role="contentinfo">
    <p>&#169; Copyright 2022, Sebastian Schwalbe.</p>
  </div>

  Built with <a href="https://www.sphinx-doc.org/">Sphinx</a> using a
    <a href="https://github.com/readthedocs/sphinx_rtd_theme">theme</a>
    provided by <a href="https://readthedocs.org">Read the Docs</a>.
   

</footer>
        </div>
      </div>
    </section>
  </div>
  <script>
      jQuery(function () {
          SphinxRtdTheme.Navigation.enable(true);
      });
  </script> 

</body>
</html>