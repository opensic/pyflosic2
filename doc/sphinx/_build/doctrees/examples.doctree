��_"      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��target���)��}�(h�.. _examples:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��examples�u�tagname�h
�line�K�parent�hhh�source��A/home/schwalbe/Programms/gitlab/pyflosic2/doc/sphinx/examples.rst�ubh	�section���)��}�(hhh]�(h	�title���)��}�(h�Examples�h]�h	�Text����Examples�����}�(hh,h h*hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h h%hhh!h"hKubh)��}�(h�.. _installing-docdir:�h]�h}�(h]�h]�h]�h]�h]�h�installing-docdir�uhh
hKh h%hhh!h"ubh$)��}�(hhh]�(h))��}�(h�Python examples�h]�h/�Python examples�����}�(hhJh hHhhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h hEhhh!h"hKubh	�	paragraph���)��}�(hX  Starting from scratch, an easy PyFLOSIC2 calculation
requires an Atoms object. This object contains
nuclei positions and chemical symbols. In addition, it takes spin and charge
as mandatory input. It can carry
explicit electron positions, which are needed for FLO-SIC
calculations. We build a cooking recipe, i.e., a Workflow, to
automatically generate these positions using the PyCOM
procedure. By specifying the target accuracy (tier_name, flevel)
you can run a simple FLO-SIC calculation with the following script�h]�h/X  Starting from scratch, an easy PyFLOSIC2 calculation
requires an Atoms object. This object contains
nuclei positions and chemical symbols. In addition, it takes spin and charge
as mandatory input. It can carry
explicit electron positions, which are needed for FLO-SIC
calculations. We build a cooking recipe, i.e., a Workflow, to
automatically generate these positions using the PyCOM
procedure. By specifying the target accuracy (tier_name, flevel)
you can run a simple FLO-SIC calculation with the following script�����}�(hhZh hXhhh!NhNubah}�(h]�h]�h]�h]�h]�uhhVh!h"hKh hEhhubh	�	container���)��}�(hhh]�(h	�caption���)��}�(h�workflow_example.py�h]�h/�workflow_example.py�����}�(hhoh hmubah}�(h]�h]�h]�h]�h]�uhhkh!h"hKh hhubh	�literal_block���)��}�(hX  from pyflosic2 import Atoms,WORKFLOW,GUI
# Atoms
sym = ['C']+4*['H']
p0 = [+0.00000000,+0.00000000,+0.00000000]
p1 = [+0.62912000,+0.62912000,+0.62912000]
p2 = [-0.62912000,-0.62912000,+0.62912000]
p3 = [+0.62912000,-0.62912000,-0.62912000]
p4 = [-0.62912000,+0.62912000,-0.62912000]
pos = [p0,p1,p2,p3,p4]
atoms = Atoms(sym,pos,spin=0,charge=0)
# WORKFLOW
wf = WORKFLOW(atoms,
              mode='unrestricted',
              tier_name='tier1',
              flevel=0,
              log_name='UWF.log')
wf.kernel()
# GUI
GUI(wf.p.atoms)�h]�h/X  from pyflosic2 import Atoms,WORKFLOW,GUI
# Atoms
sym = ['C']+4*['H']
p0 = [+0.00000000,+0.00000000,+0.00000000]
p1 = [+0.62912000,+0.62912000,+0.62912000]
p2 = [-0.62912000,-0.62912000,+0.62912000]
p3 = [+0.62912000,-0.62912000,-0.62912000]
p4 = [-0.62912000,+0.62912000,-0.62912000]
pos = [p0,p1,p2,p3,p4]
atoms = Atoms(sym,pos,spin=0,charge=0)
# WORKFLOW
wf = WORKFLOW(atoms,
              mode='unrestricted',
              tier_name='tier1',
              flevel=0,
              log_name='UWF.log')
wf.kernel()
# GUI
GUI(wf.p.atoms)�����}�(hhh h}ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve��force���language��python��highlight_args�}�uhh{h!h"hKh hhubeh}�(h]��workflow-example-py�ah]��literal-block-wrapper�ah]��workflow-example-py�ah]�h]��literal_block��uhhfh hEhhh!hhNubhW)��}�(h�kThe final geometry (wf.p.atoms) containing nuclei as well as FODs
can be visualized with the PyFLOSIC2 GUI.�h]�h/�kThe final geometry (wf.p.atoms) containing nuclei as well as FODs
can be visualized with the PyFLOSIC2 GUI.�����}�(hh�h h�hhh!NhNubah}�(h]�h]�h]�h]�h]�uhhVh!h"hK/h hEhhubeh}�(h]�(�python-examples�hDeh]�h]�(�python examples��installing-docdir�eh]�h]�uhh#h h%hhh!h"hK�expect_referenced_by_name�}�h�h:s�expect_referenced_by_id�}�hDh:subh$)��}�(hhh]�(h))��}�(h�Command-line examples�h]�h/�Command-line examples�����}�(hh�h h�hhh!NhNubah}�(h]�h]�h]�h]�h]�uhh(h h�hhh!h"hK4ubhW)��}�(hX&  PyFLOSIC2 comes with command-line tools.
You can generate FODs from a .xyz file
(--xyz to specify the file) using the --wf flag.
This flag takes one of two options: unrestricted or restricted.
If you already have a geometry containing atoms and FODs,
you can run a FLO-SIC calculations with the --run flag.
This flag takes the same options as the --wf flag.
Additional options can be given for spin (--spin),
charge (--charge), tier level (--tier), and Flevel (--flevel).
You can visualize a structure containing atoms and FODs with
the --gui option.�h]�h/X/  PyFLOSIC2 comes with command-line tools.
You can generate FODs from a .xyz file
(–xyz to specify the file) using the –wf flag.
This flag takes one of two options: unrestricted or restricted.
If you already have a geometry containing atoms and FODs,
you can run a FLO-SIC calculations with the –run flag.
This flag takes the same options as the –wf flag.
Additional options can be given for spin (–spin),
charge (–charge), tier level (–tier), and Flevel (–flevel).
You can visualize a structure containing atoms and FODs with
the –gui option.�����}�(hh�h h�hhh!NhNubah}�(h]�h]�h]�h]�h]�uhhVh!h"hK5h h�hhubhg)��}�(hhh]�(hl)��}�(h�Command-line tool�h]�h/�Command-line tool�����}�(hh�h h�ubah}�(h]�h]�h]�h]�h]�uhhkh!h"hKAh h�ubh|)��}�(h��pyflosic2 --wf unrestricted --xyz [nuclei_only].xyz
pyflosic2 --run unrestricted --xyz [nuclei_and_fods].xyz
pyflosic2 --gui [name].xyz�h]�h/��pyflosic2 --wf unrestricted --xyz [nuclei_only].xyz
pyflosic2 --run unrestricted --xyz [nuclei_and_fods].xyz
pyflosic2 --gui [name].xyz�����}�(hhh h�ubah}�(h]�h]�h]�h]�h]�h�h�h��h��shell�h�}�uhh{h!h"hKAh h�ubeh}�(h]��command-line-tool�ah]�h�ah]��command-line tool�ah]�h]��literal_block��uhhfh h�hhh!hhNubeh}�(h]��command-line-examples�ah]�h]��command-line examples�ah]�h]�uhh#h h%hhh!h"hK4ubeh}�(h]�(h�id1�eh]�h]��examples�ah]��examples�ah]�uhh#h hhhh!h"hK�
referenced�Kh�}�j  hsh�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h"uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h(N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j7  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h"�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�(h]�hahD]�h:au�nameids�}�(j  hh�hDh�h�h�h�j  j  h�h�u�	nametypes�}�(j  �h��h�Nh��j  Nh��uh}�(hh%j
  h%hDhEh�hEh�hhj  h�h�h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h	�system_message���)��}�(hhh]�hW)��}�(h�+Duplicate implicit target name: "examples".�h]�h/�/Duplicate implicit target name: “examples”.�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhhVh j�  ubah}�(h]�h]�h]�h]�h]�j
  a�level�K�type��INFO��source�h"�line�Kuhj�  h h%hhh!h"hKuba�transform_messages�]�(j�  )��}�(hhh]�hW)��}�(hhh]�h/�.Hyperlink target "examples" is not referenced.�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhhVh j�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h"�line�Kuhj�  ubj�  )��}�(hhh]�hW)��}�(hhh]�h/�7Hyperlink target "installing-docdir" is not referenced.�����}�(hhh j�  ubah}�(h]�h]�h]�h]�h]�uhhVh j�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h"�line�Kuhj�  ube�transformer�N�
decoration�Nhhub.