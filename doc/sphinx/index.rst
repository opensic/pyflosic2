.. sampledoc documentation master file, created by
   sphinx-quickstart on Sun May  3 17:06:22 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the PyFLOSIC2 documentation!
=======================================
Contents:

.. toctree::
   :numbered:
   :maxdepth: 2

   install.rst
   examples.rst
   intro.rst
   pyflosic2.rst
   literature.rst 
   api.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
