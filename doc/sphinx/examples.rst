.. _examples:


***************
Examples 
***************

.. _installing-docdir:

Python examples
===============
Starting from scratch, an easy PyFLOSIC2 calculation 
requires an Atoms object. This object contains 
nuclei positions and chemical symbols. In addition, it takes spin and charge 
as mandatory input. It can carry 
explicit electron positions, which are needed for FLO-SIC
calculations. We build a cooking recipe, i.e., a Workflow, to 
automatically generate these positions using the PyCOM 
procedure. By specifying the target accuracy (tier_name, flevel)
you can run a simple FLO-SIC calculation with the following script


.. code-block:: python
   :caption: workflow_example.py
   :name: workflow-example-py

   from pyflosic2 import Atoms,WORKFLOW,GUI
   # Atoms
   sym = ['C']+4*['H']
   p0 = [+0.00000000,+0.00000000,+0.00000000]
   p1 = [+0.62912000,+0.62912000,+0.62912000]
   p2 = [-0.62912000,-0.62912000,+0.62912000]
   p3 = [+0.62912000,-0.62912000,-0.62912000]
   p4 = [-0.62912000,+0.62912000,-0.62912000]
   pos = [p0,p1,p2,p3,p4]
   atoms = Atoms(sym,pos,spin=0,charge=0)
   # WORKFLOW
   wf = WORKFLOW(atoms,
                 mode='unrestricted',
                 tier_name='tier1',
                 flevel=0,
                 log_name='UWF.log')
   wf.kernel()
   # GUI
   GUI(wf.p.atoms)

The final geometry (wf.p.atoms) containing nuclei as well as FODs
can be visualized with the PyFLOSIC2 GUI.


Command-line examples 
=====================
PyFLOSIC2 comes with command-line tools. 
You can generate FODs from a .xyz file 
(--xyz to specify the file) using the --wf flag.
This flag takes one of two options: unrestricted or restricted.
If you already have a geometry containing atoms and FODs,
you can run a FLO-SIC calculations with the --run flag.
This flag takes the same options as the --wf flag.
Additional options can be given for spin (--spin), 
charge (--charge), tier level (--tier), and Flevel (--flevel).
You can visualize a structure containing atoms and FODs with
the --gui option.

.. code-block:: shell
   :caption: Command-line tool
   :name: Command-line tool

   pyflosic2 --wf unrestricted --xyz [nuclei_only].xyz
   pyflosic2 --run unrestricted --xyz [nuclei_and_fods].xyz 
   pyflosic2 --gui [name].xyz
