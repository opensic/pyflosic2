from pyflosic2.parameters.flosic_parameters import parameters
from pyflosic2.io.uflosic_io import atoms2pyscf
from pyflosic2.systems.uflosic_systems import CH4
from pyflosic2.guess.pycom import pycom
from pyflosic2.gui.view import GUI
from pyscf import gto, scf

# Parameters
p = parameters(mode='unrestricted')
p.init_atoms(CH4())
p.basis = 'pc0'

# PySCF: DFT
mol = gto.M(atom=atoms2pyscf(p.nuclei), 
            basis=p.basis, 
            spin=p.spin, 
            charge=p.charge, 
            symmetry=p.symmetry)
mf = scf.UKS(mol)
mf.xc = p.xc 
mf.verbose = 0
mf.kernel()

# PyCOM
pc = pycom(mf=mf, p=p)
pc.get_guess()

# GUI
GUI(pc.f_guess, p=p)

