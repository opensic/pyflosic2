from pyflosic2.io.flosic_io import atoms_from_xyz
from pyflosic2 import WORKFLOW,GUI

def pycom(f_xyz,spin, charge, tier_name):
    # xyz2atoms 
    atoms = atoms_from_xyz(f_xyz,spin=spin,charge=charge)
    # Guess generation at object generation time 
    wf = WORKFLOW(atoms,
                  mode='unrestricted',
                  tier_name=tier_name,
                  log_name='PyCOM.log')
    # Parameters
    wf.p.show() 
    # GUI 
    GUI(wf.p.atoms)

def main():
    f_xyz = 'Conformer3D_CID_6325.xyz'
    pycom(f_xyz=f_xyz,spin=0, charge=0, tier_name='tier1')

if __name__ == '__main__': 
    main()
