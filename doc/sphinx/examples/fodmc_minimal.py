from pyflosic2 import parameters
from pyflosic2.guess.pyfodmc import pyfodmc
from pyflosic2.gui.view import GUI

def fodmc_workflow(f_mol):
    # Parameters
    p = parameters()
    # fodMC 
    g = pyfodmc(p=p, input_data=f_mol)
    g.get_guess()
    # GUI 
    GUI(g.f_guess, p=p)

def main():
    f_mol = 'Conformer3D_CID_6325.mol'
    fodmc_workflow(f_mol)

if __name__ == '__main__':
    main()
