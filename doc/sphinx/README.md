# Author 
- Sebastian Schwalbe
- code: python3, pip, sphinx, html 

# Start a better documentation 
- use sphinx      
    - change conf.py 
- use sphinx_rtd_theme theme 
- make html times two 
- use  .gitlab-ci.yml 
    - copy html correctly 
    - use git pages 
- landing page is produced by index.rst 

python commands
~~~python 
pip3 install sphinx 
pip3 install sphinx_rtd_theme
pip3 install sphinxcontrib-bibtex
~~~  

shell commands
~~~
sphinx-quickstart
make html 
make html 
google-chrome _build/html/index.html 
~~~

changing the sphinx documentation
~~~
cd pyflosic2/doc/sphinx
[make some changes]
make html
make html
git add .
git commit
git push
[automatically: .gitlab_ci.yml -> pipline -> 1 - 2 mins -> new gitlab pages]
~~~

# References 
- [Matplotlib](https://matplotlib.org/sampledoc/)
- [PySCF](https://sunqm.github.io/pyscf/)
- [GPAW](https://wiki.fysik.dtu.dk/gpaw/)
