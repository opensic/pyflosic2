.. _pyflosic2:

*********
PyFLOSIC2
*********
.. image:: pics/PyFLOSIC_two.png
   :width: 200

The PyFLOSIC2 code is the successor of the PyFLOSIC code :cite:`Schwalbe2020_084104`. 
Just like PyFLOSIC, PyFLOSIC2 builds on-top of the PySCF code inheriting various features, 
e.g., access to a vast variety of exchange-correlation functionals.
In contrast to PyFLOSIC, PyFLOSIC2 uses object-oriented programming (OOP) pragmas 
to enrich its modularity, flexibility as well as applicability. 
Main features are introduced as puzzle-pieces, represented by Python classes. 
Similar to PyFLOSIC, 
the main functionality of a class is executed with a kernel function. 
PyFLOSIC2 comes with a novel handling of nuclei and electron position 
in an Atoms object. This allows to handle FLO-SIC parameters, i.e., nuclei 
as well as Fermi-orbital descriptor positions, 
more consistently and without additional dependencies, i.e., ASE. 
To enable the visualization of nuclei and electron positions, 
PyFLOSIC2 offers a GUI. 


.. image:: pics/platonic_hydrocarbons.png
   :width: 200

PyFLOSIC2 GUI: Platonic hydrocarbons. 

Features 
--------

* All LDA, GGA, meta-GGA functionals from Libxc :cite:`Lehtola2018_1` /XCfun :cite:`Ekstrom2010_1971`
* All GTO basis sets supported
* Transparent usage of PySCF features :cite:`Sun2018_e1340,Sun2020_1`
* Automatic generation of FODs (PyCOM, fodMC)
* Analytical FOD forces
* Two unified SIC Hamiltonians
* FLO-SIC total energy and eigenvalues
* Puzzle-pieces and automatic workflows 
* Command-line tools 
* Two-step SCF 
* Tier level and Flevel system 
* Visualization of Atoms object (GUI) 
* FOD bond order


SCF
---

In contrast to PyFLOSIC, a two-step self-consistent field cycle (SCF)
has been implemented in PyFLOSIC2. In the outer loop, the density
matrix (DM) is optimized based on the current FOD positions. In the 
inner loop, the FODs are optimized based on the current DM. This two-step SCF
allows to introduce different FLO-SIC levels (Flevels) as explained below.
It shall be noted that this two-step SCF is superior to the original 
implementation in PyFLOSIC by converging smoother using fewer iterations.

.. image:: pics/02_scf_b.png
   :width: 200

PyFLOSIC2 SCF: Two-step SCF.


Tier level
----------

The numerical accuracy required for SIC is different than for DFT. 
Similar to other codes, we propose a Tier level system 
for increasing numerical accuracy; however, this level system is introduced
for SIC rather than for DFT. The smallest tier levels (0,1) can be used
to quickly assess results and compare different FOD guesses. For 
production quality, larger values like 2 or 3 need to be used.

.. table:: Tier levels (tier). All tier levels use unpruned grids.

   +------+-------------+--------------+-------------------------+
   | Tier | Basis       | Grid         | Meaning                 |
   +======+=============+==============+=========================+
   | 1    | STO-3G      | grid.level=3 | code debugging          |
   +------+-------------+--------------+-------------------------+
   | 2    | pc-0        | grid.level=3 | examplary calculation   |
   +------+-------------+--------------+-------------------------+
   | 3    | pc-1        | grid.level=7 | useful calculation      |
   +------+-------------+--------------+-------------------------+
   | 4    | pc-2        | (150,1202)   | production quality      |
   +------+-------------+--------------+-------------------------+
   | 5    | aug-pc2     | (200,1454)   | high quality            |
   +------+-------------+--------------+-------------------------+
   | 6    | unc-aug-pc2 | (200,1454)   | very high quality       |
   +------+-------------+--------------+-------------------------+



Flevel 
------

The two-step SCF cycle allows to realize different 
approximations for FLO-SIC, which are called FLO-SIC leves (Flevels). 
The Flevels are a compromise between desired accuracy and computational cost. 
For example, one can optimize the density matrix on a fixed set of FODs. This
can give insights into the basic importance of SIC for the given system, and
allows to compare different starting points for the SIC calculation. 
Higher levels introduce more optimizations, where the highest level (Flevel=3)
represents a full FLO-SIC calculation in which both the DM and the FODs are fully optimized.

.. raw:: latex

   \small 

.. table:: FLO-SIC levels (Flevel). Here, init stands for the values the
           calculation is initialized with, whereas opt refers to the fact that
           a property is fully optimized. Update refers to one property
           being adjusted with respect to the other property. The meaning is
           expressed as functional dependencies.

   +--------+--------+--------+--------------------------------------------+
   | Flevel | FODs   | DM     | Meaning                                    |
   +========+========+========+============================================+
   | 0      | init   | init   | FLO(DM\ :math:`_{\text{init}}`)            |
   +--------+--------+--------+--------------------------------------------+
   | 1      | init   | update | DM(FOD\ :math:`_{\text{init}}`)            |
   +--------+--------+--------+--------------------------------------------+
   | 2      | update | update | DM(FOD(DM\ :math:`_{\text{init}}`))        |
   +--------+--------+--------+--------------------------------------------+
   | 3      | opt    | opt    | SCF(DM\ :math:`_{i}`\ (FOD\ :math:`_{j}`)) |
   +--------+--------+--------+--------------------------------------------+


Fermi-orbital descriptors 
-------------------------
The Python center of mass (PyCOM) uses the centroids of localized orbitals to generate
FOD guesses. PyCOM is included in the installation of PyFLOSIC2, as is it used as 
the standard method for FOD generation. The only user inputs are the ones which are
necessary for any DFT/SIC calculation, e.g., chemical symbols, nuclei positions, 
the charge, and the spin of the system. No further user input is required.

.. code-block:: python
   :caption: workflow_pycom.py
   :name: workflow-pycom-py

   >>> from pyflosic2 import parameters
   >>> from pyflosic2.io.flosic_io import atoms_from_xyz
   >>> from pyflosic2 import Atoms,WORKFLOW,GUI
   >>> def pycom_workflow(f_xyz,spin, charge, tier_name):
   >>>     # xyz2atoms 
   >>>     atoms = atoms_from_xyz(f_xyz,spin=spin,charge=charge)
   >>>     # Guess generation at object generation time 
   >>>     wf = WORKFLOW(atoms,
   >>>                   mode='unrestricted',
   >>>                   tier_name=tier_name,
   >>>                   log_name='PyCOM.log')
   >>>     # Parameters
   >>>     wf.p.show()
   >>>     # GUI 
   >>>     GUI(wf.p.atoms)
   >>> def main():
   >>>     f_xyz = 'Conformer3D_CID_6325.xyz'
   >>>     pycom_workflow(f_xyz=f_xyz, spin=0, charge=0, tier_name='tier1')
   >>> if __name__ == '__main__':
   >>>     main()


.. code-block:: python
   :caption: pycom_custom.py
   :name: pycom-custom-py

   >>> from pyflosic2.parameters.flosic_parameters import parameters
   >>> from pyflosic2.io.uflosic_io import atoms2pyscf
   >>> from pyflosic2.systems.uflosic_systems import CH4
   >>> from pyflosic2.guess.pycom import pycom
   >>> from pyflosic2.gui.view import GUI
   >>> from pyscf import gto, scf
   >>> # Parameters
   >>> p = parameters(mode='unrestricted')
   >>> p.init_atoms(CH4())
   >>> p.basis = 'pc0'
   >>> # PySCF: DFT
   >>> mol = gto.M(atom=atoms2pyscf(p.nuclei),
   >>>             basis=p.basis,
   >>>             spin=p.spin,
   >>>             charge=p.charge,
   >>>             symmetry=p.symmetry)
   >>> mf = scf.UKS(mol)
   >>> mf.xc = p.xc
   >>> mf.verbose = 0
   >>> mf.kernel()
   >>> # PyCOM
   >>> pc = pycom(mf=mf, p=p)
   >>> pc.get_guess()
   >>> # GUI
   >>> GUI(pc.f_guess, p=p)
    

The Fermi-orbital descriptor Monte-Carlo (fodMC) code generates FOD positions
based on pre-defined motifs which the FODs typically obey. Such motifs can be 
found for bond, lone, and core FODs. The optimization of these motifs is partially
done via Monte-Carlo. Any initial bonding configuration, e.g., single, double, or triple bonds,
can be initialized using the fodMC.
However, the user needs to provide this bonding information in addition to 
chemical symbols and positions of the nuclei. 
Setting bonding information by hand become quite quickly impractical for larger systems. 
Therefore, the new pyfodmc can generate FODs from typical chemical bonding files, i.e., mol files. 


.. code-block:: python
   :caption: workflow_pyfodmc.py
   :name: workflow-pyfodmc-py

   >>> from pyflosic2 import parameters
   >>> from pyflosic2.guess.pyfodmc import pyfodmc
   >>> from pyflosic2.gui.view import GUI
   >>> def fodmc_workflow(f_mol):
   >>>     # Parameters
   >>>     p = parameters()
   >>>     # fodMC 
   >>>     g = pyfodmc(p=p, input_data=f_mol)
   >>>     g.get_guess()
   >>>     # GUI 
   >>>     GUI(g.f_guess, p=p)
   >>> def main():
   >>>     f_mol = 'Conformer3D_CID_6325.mol'
   >>>     fodmc_workflow(f_mol)
   >>> if __name__ == '__main__':
   >>>     main()


