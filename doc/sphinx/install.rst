.. _installation:


***************
Installation 
***************

.. _installing-docdir:

PySCF
=====
PyFLOSIC2 is built on top of PySCF, thus you need 
to install PySCF 

.. code-block:: bash

   $ pip3 install --upgrade pyscf




PyFLOSIC2
=========
PyFLOSIC2 can be easily installed using pip

.. code-block:: bash

   $ pip3 install pyflosic2

 
or 


.. code-block:: bash

   $ git clone https://gitlab.com/opensic/pyflosic2.git
   $ cd pyflosic2
   $ pip3 install -e .

